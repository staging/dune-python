Upgrade guide from dune-corepy to dune-python
=============================================

As decided at the 2016 developer meeting dune-corepy has been renamed to
dune-python. To upgrade a cloned git repository enter

~~~
git remote set-url origin ssh://git@gitlab.dune-project.org:22022/staging/dune-python.git
~~~

(trying `git pull` in the repository will also give you this command).

Remarks
-------

- The original dune-corepy master is still available in the new module as
  branch

~~~
  git checkout corepy
~~~

- Other modules (e.g. dune-alugrid) should either work as before or have a
  corresponding `corepy` branch (e.g. dune-fempy).
- The name change had to be accompanied by changes to the directory
  structure, include files, and the C++ namespace. Therefore, a complete
  rebuild of dune-python (f.k.a dune-corepy) and all depended dune modules
  is required
- dune-py should be removed.
- Together with renaming we have upgraded to  pybind11 v2.2.1. Please
  consult
  http://pybind11.readthedocs.io/en/stable/upgrade.html
  for an upgrade guide for pybind11.
- The python dune namespace packages also need a complete upgrade. This
  should work by entering

~~~
  pip install --upgrade .
~~~

  in the `python` subdirectory of the build directory of each module exporting
  python binding (possibly adding `--user` depending on your setup)