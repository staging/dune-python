import numpy
from dune.grid import gridFunction, structuredGrid

gridView = structuredGrid([0,0],[1,1],[10,10])


fcalls = 0
@gridFunction(gridView)
def f(x):
    global fcalls
    fcalls += 1
    return x[0]*x[1]
gcalls = 0
@gridFunction(gridView)
def g(e,x):
    global gcalls
    gcalls += 1
    return e.geometry.toGlobal(x)

e = gridView.elements.__next__()
xLoc = numpy.array([[0,0.1,0.2,0.3],[0,0.4,0.6,0.8]])
xGlb = e.geometry.toGlobal(xLoc)

fcalls = 0
gcalls = 0
y=f(xGlb)
# print( y, fcalls)
assert fcalls == 1
y = g(e, xLoc)
# print( y, gcalls)
assert gcalls == 1

fcalls = 0
y = f(e,xLoc)
# print( y, fcalls)
assert fcalls == 1

fcalls = 0
gcalls = 0
lf = f.localFunction()
lg = g.localFunction()
lf.bind(e)
lg.bind(e)
y = lf(xLoc)
# print( y, fcalls)
assert fcalls == 1
y = lg(xLoc)
# print( y, gcalls)
assert gcalls == 1
lg.unbind()
lf.unbind()
