#include <cstddef>
#include <iostream>

#include <dune/common/fvector.hh>
// #include <dune/python/grid/entity.hh>
#include <dune/python/pybind11/pybind11.h>
#include <dune/python/pybind11/numpy.h>

template< class GridView, class Rules, class GF >
double l2norm2 ( const GridView &gridView, const Rules &rules, const GF& gf )
{
  auto lf = localFunction( gf );
  double l2norm2 = 0;
  for( const auto &entity : elements( gridView ) )
  {
    const auto geo = entity.geometry();
    typedef typename decltype(geo)::LocalCoordinate LocalCoordinate;
    lf.bind( entity );
    pybind11::object pyrule = rules( geo.type() );
    pybind11::object pyPW = pyrule.attr("get")();
    auto pointsWeights = pyPW.template cast<
       std::pair<pybind11::array_t<double>,
                 pybind11::array_t<double>> >();

    const auto &valuesArray = lf( pointsWeights.first ).template cast< pybind11::array_t< double > >();
    // check shape here...
    auto values = valuesArray.template unchecked< 1 >();
    for( std::size_t i = 0, sz = pointsWeights.second.size(); i < sz; ++i )
    {
      LocalCoordinate hatx(0);
      for (std::size_t c=0;c<LocalCoordinate::size();++c)
        hatx[c] = pointsWeights.first.at(c,i);
      double weight = pointsWeights.second.at( i ) * geo.integrationElement( hatx );
#if 0
      std::cout << hatx << "  " << pointsWeights.second.at( i )
        << "     " << weight << " " << geo.global(hatx)
        << "     " << values[i] << std::endl;
#endif
      l2norm2 += (values[ i ] * values[ i ]) * weight;
    }
    lf.unbind();
  }
  return l2norm2;
}
