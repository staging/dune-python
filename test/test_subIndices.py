from dune.grid import structuredGrid

grid = structuredGrid([0,0], [1,1], [2,2])
indexSet = grid.indexSet

for intersection in grid.boundaryIntersections:
    entity      = intersection.inside
    subentity   = (intersection.indexInInside, 1)

    indices_global      = indexSet.subIndices(entity, subentity, 2)
    indices_reference   = entity.referenceElement.subEntities(subentity, 2)
    indices_lookup      = indexSet.subIndices(entity, 2)

    assert len(indices_global) == len(indices_reference)

    for i, j in zip(indices_global, indices_reference):
        assert i == indices_lookup[j]
