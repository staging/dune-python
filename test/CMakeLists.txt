add_executable(test_eval EXCLUDE_FROM_ALL test_eval.cc)
target_link_libraries(test_eval ${DUNE_LIBS} ${PYTHON_LIBRARIES})

dune_symlink_to_source_files(FILES test_eval_call.py myfunc.hh myclass.hh test_quad.hh
refelement.py test_gf.py bcrsmatrix.py test_subIndices.py geometrytype.py test_class_export.py gf_evaluate.py
test_eval_call.py
)

dune_python_add_test(NAME combinedtests
                     COMMAND ${PYTHON_EXECUTABLE} combinedtests.py
                     WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
# dune_python_add_test(NAME refelement
#                      COMMAND ${PYTHON_EXECUTABLE} refelement.py
#                      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
# dune_python_add_test(NAME test_gf
#                      COMMAND ${PYTHON_EXECUTABLE} test_gf.py
#                      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
# dune_python_add_test(NAME bcrsmatrix
#                      COMMAND ${PYTHON_EXECUTABLE} bcrsmatrix.py
#                      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
# dune_python_add_test(NAME test_subIndices
#                      COMMAND ${PYTHON_EXECUTABLE} test_subIndices.py
#                      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
# dune_python_add_test(NAME geometrytype
#                      COMMAND ${PYTHON_EXECUTABLE} geometrytype.py
#                      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
# dune_python_add_test(NAME test_class_export
#                      COMMAND ${PYTHON_EXECUTABLE} test_class_export.py
#                      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
# dune_python_add_test(NAME gf_evaluate
#                      COMMAND ${PYTHON_EXECUTABLE} gf_evaluate.py
#                      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
# dune_python_add_test(NAME test_eval_call
#                      COMMAND ${PYTHON_EXECUTABLE} test_eval_call.py
#                      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
                   # dune_python_add_test(NAME test_quad
                   #                     COMMAND ${PYTHON_EXECUTABLE} test_quad.py
                   #                      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
