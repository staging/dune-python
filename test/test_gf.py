import time, math, numpy
import dune
from dune.common import FieldVector

from dune.grid import structuredGrid, gridFunction, GridFunction

grid = structuredGrid([0, 0], [1, 1], [12, 12])

gf1 = grid.function(lambda e,x:\
          math.sin(math.pi*(e.geometry.toGlobal(x)[0]+e.geometry.toGlobal(x)[1])))
assert gf1.name == "tmp0"

if True:
    a = 2.
    gf1 = grid.function(lambda e,x:\
            math.sin(a*math.pi*(e.geometry.toGlobal(x)[0]+e.geometry.toGlobal(x)[1])),
            name="gf1")
    lgf1 = gf1.localFunction()
    average1 = 0
    for e in grid.elements:
        lgf1.bind(e)
        average1 += lgf1([0.5,0.5])*e.geometry.volume
    # print(average1)
    # gf1.plot()
    gf2 = grid.function("myFunction","myfunc.hh",a,name="gf2")
    lgf2 = gf2.localFunction()
    average2 = 0
    for e in grid.elements:
        lgf2.bind(e)
        average2 += lgf2([0.5,0.5])*e.geometry.volume
    # print(average2)
    # gf2.plot()
    assert abs(average1-average2)<1e-12
    diff = 0
    for e in grid.elements:
        lgf1.bind(e)
        lgf2.bind(e)
        diff += abs(lgf1([0.5,0.5])-lgf2([0.5,0.5]))
    assert diff<1e-12

if True:
    gf1 = grid.function(lambda e,x:\
            [math.sin(2*math.pi*(e.geometry.toGlobal(x)[0]+e.geometry.toGlobal(x)[1])),\
             (e.geometry.toGlobal(x)[0]-0.5)*2,0,0],
            name="gf1")
    lgf1 = gf1.localFunction()
    average1 = 0
    for e in grid.elements:
        lgf1.bind(e)
        average1 += sum(lgf1([0.5,0.5]))*e.geometry.volume
    # print(average1)
    # gf1.plot()
    a = FieldVector([2])
    gf2 = grid.function("myVecFunction","myfunc.hh",a,gf1,name="gf2")
    lgf2 = gf2.localFunction()
    average2 = 0
    for e in grid.elements:
        lgf2.bind(e)
        average2 += sum(lgf2([0.5,0.5]))*e.geometry.volume
    # print(average2)
    # gf2.plot()
    assert abs(average1-average2)<1e-12
    diff = 0
    for e in grid.elements:
        lgf1.bind(e)
        lgf2.bind(e)
        diff += abs(lgf1([0.5,0.5]).two_norm-lgf2([0.5,0.5]).two_norm)
    assert diff<1e-12
    a[0] = 3
    diff = 0
    for e in grid.elements:
        lgf1.bind(e)
        lgf2.bind(e)
        v = lgf1([0.5,0.5])
        v[1] *= 3./2.
        diff += abs(v.two_norm-lgf2([0.5,0.5]).two_norm)
    assert diff<1e-12

    grid.writeVTK("test_gf",pointdata=[gf1,gf2])
if True:
    a = 2.
    @gridFunction(grid)
    def gf1(e,x):
        return math.sin(a*math.pi*(e.geometry.toGlobal(x)[0]+e.geometry.toGlobal(x)[1]))
    lgf1 = gf1.localFunction()
    average1 = 0
    for e in grid.elements:
        lgf1.bind(e)
        average1 += lgf1([0.5,0.5])*e.geometry.volume
    # print(average1)
    # gf1.plot()
    @gridFunction(grid)
    def gf2(x):
        return math.sin(a*math.pi*(x[0]+x[1]))
    gf2 = grid.function("myFunction","myfunc.hh",a,name="gf2")
    lgf2 = gf2.localFunction()
    average2 = 0
    for e in grid.elements:
        lgf2.bind(e)
        average2 += lgf2([0.5,0.5])*e.geometry.volume
    # print(average2)
    # gf2.plot()
    assert abs(average1-average2)<1e-12
    diff = 0
    for e in grid.elements:
        lgf1.bind(e)
        lgf2.bind(e)
        diff += abs(lgf1([0.5,0.5])-lgf2([0.5,0.5]))
    assert diff<1e-12
