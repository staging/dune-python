import dune.grid
import dune.alugrid

from dune.grid import cartesianDomain, gridFunction

domain   = dune.grid.cartesianDomain([0,0,0],[2.5,2.5,2.5],[5,5,5])


print("testing ALUCubeGrid",flush=True)
print("===================",flush=True)
gridView = dune.alugrid.aluCubeGrid(domain)
pc0 = gridView.hierarchicalGrid.persistentContainer(0,1);
print("PC<0> created",pc0.size,flush=True)
pc1 = gridView.hierarchicalGrid.persistentContainer(1,1);
print("PC<1> created",pc1.size,flush=True)

print("testing UGGrid",flush=True)
print("==============",flush=True)
gridView = dune.grid.ugGrid(domain)
pc0 = gridView.hierarchicalGrid.persistentContainer(0,1);
print("PC<0> created",pc0.size,flush=True)
pc3 = gridView.hierarchicalGrid.persistentContainer(3,1);
print("PC<3> created",pc3.size,flush=True)
pc1 = gridView.hierarchicalGrid.persistentContainer(1,1);
print("PC<1> created",pc1.size,flush=True)
pc2 = gridView.hierarchicalGrid.persistentContainer(2,1);
print("PC<2> created",pc2.size,flush=True)
