#!/bin/bash

if test -e dune.module ; then
  echo "Error: Cannot execute $0 within another DUNE module, like dune-python itself."
  exit 1
fi

git clone -b master https://gitlab.dune-project.org/core/dune-common.git
git clone -b master https://gitlab.dune-project.org/core/dune-geometry.git
git clone -b master https://gitlab.dune-project.org/core/dune-grid.git
git clone -b master https://gitlab.dune-project.org/extensions/dune-alugrid.git
git clone -b master https://gitlab.dune-project.org/staging/dune-python.git

CMAKE_FLAGS=" \
  -DBUILD_SHARED_LIBS=TRUE \
  -DCMAKE_BUILD_TYPE=Release \
  -DDUNE_GRID_EXPERIMENTAL_GRID_EXTENSIONS=TRUE \
  -DENABLE_HEADERCHECK=OFF \
  -DCMAKE_DISABLE_FIND_PACKAGE_LATEX=TRUE \
  -DCMAKE_DISABLE_DOCUMENTATION=TRUE \
"
echo "CMAKE_FLAGS=\"${CMAKE_FLAGS}\"" > config.opts

./dune-common/bin/dunecontrol --opts=config.opts all

export DUNE_CMAKE_FLAGS=${CMAKE_FLAGS}
export DUNE_CONTROL_PATH=${PWD}
export PYTHONPATH=${PWD}/dune-python/build-cmake/python:${PWD}/dune-alugrid/build-cmake/python
# export DUNE_PY_DIR=/tmp/dune-py

cd dune-python/demo
python test.py

echo "Set the following environment variables, i.e., for bash"
echo "  export DUNE_CONTROL_PATH=${DUNE_CONTROL_PATH}"
echo "  export PYTHONPATH=${PYTHONPATH}"
echo "  export DUNE_CMAKE_FLAGS=\"${CMAKE_FLAGS}"\"
echo "Then try out"
echo "  python grid-demo.py"
echo "in dune-python/demo or have a look at the jupyter notebooks in"
echo "dune-python/notebooks."
echo "For just in time compilation and caching of generated Python modules"
echo "a dune module needs to be creates by default that will be placed in"
echo "${HOME}/.cache/dune-py."
echo "To choose another location set the DUNE_PY_DIR environment variable."
echo
echo "If you are using additional Dune modules with Python support you need"
echo "to add those to the PYTHONPATH as shown above"
